#!/usr/bin/python
# coding=utf-8
# SSM Sika's Server Manager
# author: Ondrej Sika
#         http://ondrejsika.com
#         sika.ondrej@gmail.com

import os
import shutil
import hashlib

from ssmconf import *
from ssmtemplates import *

def f(path, ot, content):
    f = file(path, ot)
    f.write(content)
    f.close()

def fw(path, content):
    f(path, "w", content)

def fa(path, content):
    f(path, "a", content)

def name2key(name):
    return name.replace("-", "_").replace(".", "__")

def key2name(key):
    return key.replace("__", ".").replace("_", "-")

def mysql(cmd, user=MYSQL_USER, passwd=MYSQL_PASSWD):
    cmd = 'mysql -u %s -p%s -e "%s"'%(user, passwd, cmd)
    os.system(cmd)

class Domain:
    def __init__(self, name):
        self.name = name.replace("www.", "").replace("www", "")
        self.key = name2key(self.name)

        self.user = self.key
        
        self.root = "/home/%s" % self.key
        self.home = "/home/%s/data/" % self.key
        self.www = "/home/%s/data/www" % self.key
        self.etc = "/home/%s/etc" % self.key

        self.name_nginx_config = self.key+".nginx.conf"
        self.path_nginx_config = self.etc+"/"+self.name_nginx_config

        self.name_apache2_config = self.key+".apache2.conf"
        self.path_apache2_config = self.etc+"/"+self.name_apache2_config

        self.mysql_user = self.generate_mysql_user()

    def generate_mysql_user(self):
        if len(self.user) <= 16:
            return self.user
        else:
            return "%s%s" % (self.user[:10], hashlib.sha1(self.user).hexdigest()[:6])

    def create_dir_structure(self):
        os.mkdir(self.root)
        os.mkdir(self.home)
        os.mkdir(self.www)
        os.mkdir(self.etc)

    def create_system_user(self):
        os.system("useradd -s /bin/bash -d %s %s" % (self.home, self.user, ))

    def set_own(self, path):
        os.system("chown %s %s -R"%(self.user, path))

    def set_own_www(self):
        self.set_own(self.home)

    def set_password(self, passwd):
        os.system("echo '%s\n%s' | passwd %s"%(passwd, passwd, self.user))

    def create_nginx_config(self):
        fw(self.path_nginx_config, NGINX_CONF_TEMPLATE%{"root":self.www, "domain":self.name})

    def activate_nginx_config(self):
        os.symlink(self.path_nginx_config, NGINX_SITES_ENABLED+self.name_nginx_config)
        os.system(NGINX_RESTART)

    def create_apache2_config(self):
        fw(self.path_nginx_config, APACHE2_CONF_TEMPLATE%{"root":self.www, "domain":self.name})

    def activate_apache2_config(self):
        os.symlink(self.path_nginx_config, APACHE2_SITES_ENABLED+self.name_nginx_config)
        os.system(APACHE2_RESTART)

    def delete(self):
        os.unlink(NGINX_SITES_ENABLED+self.name_nginx_config)
        os.system("userdel %s" % (self.user))
        shutil.rmtree(self.root)
        os.system(NGINX_RESTART)

    def is_exist(self):
        return os.path.exists(self.root)

    def mysql_set_password(self, passwd):
        mysql("SET PASSWORD FOR  '%s'@'%%' = PASSWORD(  '%s' )"%(self.mysql_user, passwd, ))

    def mysql_create_user(self, passwd):
        mysql("CREATE USER '%s'@'%%' IDENTIFIED BY  '%s'; GRANT USAGE ON * . * TO '%s'@'%%' IDENTIFIED BY  '%s' WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0 ; GRANT ALL PRIVILEGES ON  \`%s_%%\` . * TO '%s'@'%%';"%(self.mysql_user, passwd, self.mysql_user, passwd, self.mysql_user, self.mysql_user))

