APACHE2_CONF_TEMPLATE = """<VirtualHost *:80>
ServerName %(domain)s
ServerAlias www.%(domain)s
DocumentRoot %(root)s
<Directory %(root)s>
Options Indexes FollowSymLinks MultiViews
AllowOverride  All
Order allow,deny
allow from all
</Directory>
</VirtualHost>"""

NGINX_CONF_TEMPLATE = """server {
    listen 80; 
    server_name %(domain)s www.%(domain)s;
    
    access_log /var/log/nginx/%(domain)s.access.log;
    error_log /var/log/nginx/%(domain)s.error.log;

    root %(root)s;

    location / { 
        index  index.html index.htm index.php;
        fastcgi_store_access   user:rw  group:rw  all:r;
        autoindex on; 
    }

    location ~ \.php$ {
        location ~ \..*/.*\.php$ {return 404;}
        include fastcgi_params;
        fastcgi_pass  127.0.0.1:9000;
    }
}
"""