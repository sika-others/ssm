# ssm

## Authors

* Ondrej Sika, <http://ondrejsika.com>, <ondrej@ondrejsika.com>

## Documentation

### Commands

```
ssm add [domain] [[passwd]]
ssm rm [domain]
ssm ls 
ssm addmysql [domain] [passwd]
ssm passwd [domain] [passwd]
ssm passwdmysql [domain] [passwd]
```