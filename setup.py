#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name = "ssm",
    version = "1.0.0",
    url = 'https://github.com/phphost/ssm',
    license = 'GNU LGPL v.3',
    author = 'Ondrej Sika',
    author_email = 'ondrej@ondrejsika.com',
    scripts = [
        "ssm",
    ],
    py_modules = [
        "ssmconf",
        "ssmcore",
        "ssmtemplates",
        "libssm",
    ],
)
