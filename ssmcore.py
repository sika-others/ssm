#!/usr/bin/python
# coding=utf-8
# SSM Sika's Server Manager
# author: Ondrej Sika
#         http://ondrejsika.com
#         sika.ondrej@gmail.com

import os
import libssm

def rm_domain(domain):
    domain = libssm.Domain(domain)
    if not domain.is_exist(): quit("domain does not exist")
    domain.delete()

def create_domain(domain):
    domain = libssm.Domain(domain)
    if domain.is_exist(): quit("domain is allready exist")
    domain.create_dir_structure()
    domain.create_system_user()
    domain.set_own_www()
    domain.create_apache2_config()
    domain.activate_apache2_config()

def create_mysql(domain, passwd):
    domain = libssm.Domain(domain)
    if not domain.is_exist(): quit("domain does not exist")
    domain.mysql_create_user(passwd)


def ls():
    for directory in os.listdir("/home"):
        if directory in (".", ".."): continue
        print ssm.key2name(directory)

def set_password(domain, passwd):
    domain = libssm.Domain(domain)
    if not domain.is_exist(): quit("domain does not exist")
    domain.set_password(passwd)

def mysql_set_password(domain, passwd):
    domain = libssm.Domain(domain)
    if not domain.is_exist(): quit("domain does not exist")
    domain.mysql_set_password(passwd)

def help():
    return """
SSM Sika's Server Manager
author: Ondrej Sika
        http://ondrejsika.com
        sika.ondrej@gmail.com

commands:
ssm add [domain] [[passwd]]
ssm rm [domain]
ssm ls 
ssm addmysql [domain] [passwd]
ssm passwd [domain] [passwd]
ssm passwdmysql [domain] [passwd]
    """